FROM rootproject/root:latest

RUN  apt-get update -q\
  # folder viewer
  && apt-get install -yqq pcmanfm \
        # viewer util
        xterm \
        # git
        git-all \
        # for geant4 GEANT4_USE_QT=ON
        libcoin-dev \
        libsoqt-dev \
        # for GEANT4_USE_GDML=ON
        libxerces-c-dev

### Geant4 installation (see https://geant4-userdoc.web.cern.ch/UsersGuides/InstallationGuide/html/installguide.html#buildandinstall)
RUN cd $HOME/ \
      && mkdir Geant4 \
      && cd Geant4 \
      && wget https://gitlab.cern.ch/geant4/geant4/-/archive/v10.7.4/geant4-v10.7.4.tar.gz \
      && tar -xzvf geant4-v10.7.4.tar.gz \
      && rm -rf geant4-v10.7.4.tar.gz \
      && mkdir geant4-v10.7.4-build \
      && cd geant4-v10.7.4-build/ \
      # Here are included some Cmake options for GUI and for edep-sim 
      # cmake -DCMAKE_CXX_STANDARD=17 -DCMAKE_INSTALL_PREFIX=~/Geant4/geant4-v10.7.4-install -DGEANT4_INSTALL_DATA=ON -DGEANT4_USE_QT=ON -DGEANT4_USE_OPENGL_X11=ON -DGEANT4_BUILD_MULTITHREADED=ON -DGEANT4_INSTALL_DATA_TIMEOUT=24400 -DGEANT4_USE_GDML=ON -DGEANT4_INSTALL_DATA=ON ~/Geant4/geant4-v10.7.4
      && cmake -DCMAKE_INSTALL_PREFIX=$HOME/Geant4/geant4-v10.7.4-install \
            -DGEANT4_INSTALL_DATA=ON \
            # GUI require
            -DGEANT4_USE_QT=ON \
            -DGEANT4_USE_OPENGL_X11=ON \
            # speed and download's lags fix 
            -DGEANT4_BUILD_MULTITHREADED=ON \
            -DGEANT4_INSTALL_DATA_TIMEOUT=24400 \
            # edep-sim require
            -DGEANT4_USE_GDML=ON \
            -DGEANT4_INSTALL_DATA=ON \
            -DCMAKE_CXX_STANDARD=17 $HOME/Geant4/geant4-v10.7.4\
      # next line will cost some time
      && make -j8 \
      && echo "" \
      && echo "G4 Maked" \
      && echo "" \
      && make install \
      && echo "" \
      && echo "G4 installation have DONE" \
      && echo "" \
      && cd $HOME/Geant4/geant4-v10.7.4-install/bin/ \
      && chmod +x geant4.sh \
      && ./geant4.sh 
## test example creation (see https://dev.asifmoda.com/geant4/zapusk-primera)
RUN cd $HOME/Geant4 \
      && mkdir projects \
      && mkdir projects/test_1 \
      && mkdir projects/test_1-build \
      # you can choose any example in geant4-v10.7.4/examples folder
      && cp -r $HOME/Geant4/geant4-v10.7.4/examples/basic/B1/. ./projects/test_1/ \
      && cd projects/test_1-build \
      && cmake -DCMAKE_CXX_STANDARD=17 -DGeant4_DIR=$HOME/Geant4/geant4-v10.7.4-build/ ../test_1 \
      && make -j8      
      #to run example use ./exampleB1 inside test_1-build
### edep-sim install:
RUN cd $HOME/ \
      && mkdir edep-sim-directory \
      && cd edep-sim-directory \
      && git clone https://github.com/ClarkMcGrew/edep-sim.git \
      && mkdir edep-sim-build \
      && cd edep-sim-build \
      && cmake -DCMAKE_CXX_STANDARD=17 -DCMAKE_INSTALL_PREFIX=$HOME/edep-sim-directory/edep-sim-install $HOME/edep-sim-directory/edep-sim \
      && make -j8 \      
      && echo "" \
      && echo "edep-sim Maked" \
      && echo "" \
      && make doc \
      && make install \
      && echo "" \
      && echo "edep-sim installation have DONE" \
      && echo "" 
### create sh file for path and library path update
RUN echo "source $HOME/Geant4/geant4-v10.7.4-install/bin/geant4.sh" >> $HOME/edep-sim-directory/edep-sim.sh \
      && echo "export PATH=$PATH:$HOME/edep-sim-directory/edep-sim-install/bin" >> $HOME/edep-sim-directory/edep-sim.sh \
      && echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/edep-sim-directory/edep-sim-install/lib" >> $HOME/edep-sim-directory/edep-sim.sh \
      && cd $HOME/edep-sim-directory/ \
      && chmod +x edep-sim.sh \
      && ./edep-sim.sh

      
ENV DISPLAY=host.docker.internal:0.0
CMD pcmanfm